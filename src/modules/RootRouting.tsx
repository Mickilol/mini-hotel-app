import * as React from 'react';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import DashboardRouting from './Dashboard/DashboardRouting';
import Loader from './Shared/modules/Loader/containers';
import ModalContainer from './Modals/containers/ModalContainer';
import { lazyInject } from './IoC';
import { LoginStore } from './Authorization/modules/Login/LoginStore';
import AuthorizationRouting from './Authorization/AuthorizationRouting';
import { observer } from 'mobx-react';

@observer
class RootRouting extends React.Component<any> {

  @lazyInject(LoginStore)
  private readonly store: LoginStore;

  render() {
    const canAccessDashboard = this.store.canAccessDashboard;

    return (
      <Loader>
        <ModalContainer />
        <Switch>

          <Route path='/authorization' render={() => (
            !canAccessDashboard
              ? <AuthorizationRouting />
              : <Redirect to='/'/>
          )}/>

          <Route path='/' render={() => (
            canAccessDashboard
              ? <DashboardRouting />
              : <Redirect to='/authorization/login'/>
          )} />

        </Switch>
      </Loader>
    );
  }
}

export default withRouter(RootRouting);