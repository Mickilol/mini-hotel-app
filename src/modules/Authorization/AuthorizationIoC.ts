import { LoginStore } from './modules/Login/LoginStore';
import { RegisterStore } from './modules/Register/RegisterStore';


export const authorizationSingletons = [
  LoginStore,
  RegisterStore,
];