import * as React from 'react';
import { Redirect, Route, RouteComponentProps, Switch, withRouter } from 'react-router-dom';
import LoginContainer from './modules/Login/LoginContainer';
import RegisterContainer from './modules/Register/RegisterContainer';

class AuthorizationRouting extends React.Component<RouteComponentProps> {
  render() {
    return (
      <Switch>
        <Route exact path='/authorization/login' component={LoginContainer}/>
        <Route exact path='/authorization/register' component={RegisterContainer}/>

        <Redirect to='/authorization/login' />
      </Switch>
    );
  }
}

export default withRouter(AuthorizationRouting);