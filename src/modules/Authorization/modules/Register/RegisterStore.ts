import { action, computed, observable } from 'mobx';
import { inject, injectable } from 'inversify';
import { LoaderStore } from '../../../Shared/modules/Loader/store/LoaderStore';
import { AxiosWrapper } from '../../../Shared/services/AxiosWrapper';
import { AxiosResponse } from 'axios';
import RegisterContainer from './RegisterContainer';

export interface IRegisterPayload {
  login: string;
  password: string;
  lastName: string;
  name: string;
  secondName: string;
  passportDetails: string;
  phone: string;
}

@injectable()
export class RegisterStore {
  protected static readonly REGISTER_TASK = 'REGISTER_TASK';

  @inject(AxiosWrapper)
  private readonly axiosWrapper: AxiosWrapper;

  @inject(LoaderStore)
  private readonly loaderStore: LoaderStore;

  @observable isLoginOrPhoneIncorrect = false;

  @action
  registerAttempt(payload: IRegisterPayload) {
    this.loaderStore.addTask(RegisterStore.REGISTER_TASK);

    this.axiosWrapper.post('/account/signin/', payload)
      .then(value => {
        this.loaderStore.removeTask(RegisterStore.REGISTER_TASK);
      })
      .catch((reason: AxiosResponse) => {
        this.isLoginOrPhoneIncorrect = true;

        this.loaderStore.removeTask(RegisterStore.REGISTER_TASK);
      });
  }

  @action
  reset() {
    this.isLoginOrPhoneIncorrect = false;

    this.loaderStore.removeTask(RegisterStore.REGISTER_TASK);
  }
}