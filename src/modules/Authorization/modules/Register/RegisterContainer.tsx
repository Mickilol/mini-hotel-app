import * as React from 'react';
import ModalBase from '../../../Shared/components/Modal/ModalBase';
import Input from '../../../Shared/components/Inputs/Input';
import Button from '../../../Shared/components/Buttons/Button';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { observer } from 'mobx-react';
import { lazyInject } from '../../../IoC';
import { IOnChangeProps } from '../../../Shared/types/IChangeProps';
import { IRegisterPayload, RegisterStore } from './RegisterStore';


interface ILoginContainerState {
  login: string;
  password: string;
  lastName: string;
  name: string;
  secondName: string;
  passportDetails: string;
  phone: string;
}

@observer
class RegisterContainer extends React.Component<RouteComponentProps, ILoginContainerState> {
  @lazyInject(RegisterStore)
  readonly loginStore: RegisterStore;

  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: '',
      lastName: '',
      name: '',
      secondName: '',
      passportDetails: '',
      phone: '',
    };
  }

  componentWillUnmount() {
    this.loginStore.reset();
  }

  handleChange = ({name, value}: IOnChangeProps) => {
    this.setState({[name]: value} as any);
    this.loginStore.reset();
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const { login, password, lastName, name, secondName, passportDetails,  phone } = this.state;
    const payload = { login, password, lastName, name, secondName, passportDetails, phone } as IRegisterPayload;

    this.loginStore.registerAttempt(payload);
  };

  handleClose = () => {
    this.props.history.push('/authorization/login');
  };

  render() {
    const { isLoginOrPhoneIncorrect } = this.loginStore;

    return (
      <ModalBase isOpen disableOverlay className='modal__login' onRequestClose={this.handleClose} hideCloseIcon={true}>
        <h2 className='modal__title'>
          Регистрация
        </h2>
        <form onSubmit={this.handleSubmit}>
          <Input
            name='login'
            label='Логин'
            placeholder='Введите ваш логин'
            errorMessage='Логин или телефон уже занят'

            onChange={this.handleChange}
            showError={isLoginOrPhoneIncorrect}
          />

          <Input
            name='password'
            type='password'
            label='Пароль'
            placeholder='Введите ваш пароль'

            onChange={this.handleChange}
          />

          <Input
            name='lastName'
            label='Фамилия'
            placeholder='Введите вашу фамилию'

            onChange={this.handleChange}
          />

          <Input
            name='name'
            label='Имя'
            placeholder='Введите ваше имя'

            onChange={this.handleChange}
          />

          <Input
            name='secondName'
            label='Отчество'
            placeholder='Введите ваше отчество'

            onChange={this.handleChange}
          />

          <Input
            name='passportDetails'
            mask='9999 999999'
            label='Паспортные данные'
            placeholder='Введите серию и номер паспорта'

            onChange={this.handleChange}
          />

          <Input
            name='phone'
            mask='9-999-999-99-99'
            label='Телефон'
            placeholder='Введите ваш телефон'
            errorMessage='Логин или телефон уже занят'

            onChange={this.handleChange}
            showError={isLoginOrPhoneIncorrect}
          />

          <Button className='dashboard-btn dashboard-btn--modal' type='submit'>
            Зарегистрироваться
          </Button>
        </form>


        <p className='modal__login__sign-up no-margin text-center'>
          Есть аккаунт?
          <br/>
          <Link to='/authorization/login'>
            <span className='clickable-label'>Авторизоваться!</span>
          </Link>
        </p>
      </ModalBase>
    );
  }

}

export default withRouter(RegisterContainer);