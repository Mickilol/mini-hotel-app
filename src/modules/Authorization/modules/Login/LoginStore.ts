import { action, computed, observable } from 'mobx';
import { inject, injectable } from 'inversify';
import { LoaderStore } from '../../../Shared/modules/Loader/store/LoaderStore';
import { AxiosWrapper } from '../../../Shared/services/AxiosWrapper';
import { AxiosResponse } from 'axios';

export interface ILoginPayload {
  login: string;
  password: string;
}

@injectable()
export class LoginStore {
  protected static readonly LOGIN_ATTEMPT_TASK = 'LOGIN_ATTEMPT_TASK';

  @inject(AxiosWrapper)
  private readonly axiosWrapper: AxiosWrapper;

  @inject(LoaderStore)
  private readonly loaderStore: LoaderStore;

  @observable isLoginOrPasswordIncorrect = false;
  @observable isLoggedIn = sessionStorage.getItem('isLoggedIn') === 'yes';
  @observable accountId = Number(sessionStorage.getItem('accountId'));
  @observable isAdmin = sessionStorage.getItem('isAdmin') === 'true';

  @computed
  get canAccessDashboard() {
    return this.isLoggedIn;
  }

  @action
  loginAttempt(payload: ILoginPayload) {
    this.loaderStore.addTask(LoginStore.LOGIN_ATTEMPT_TASK);

    this.axiosWrapper.post('/account/login/', payload)
      .then(value => {
        this.loaderStore.removeTask(LoginStore.LOGIN_ATTEMPT_TASK);

        sessionStorage.setItem('isLoggedIn', 'yes');
        this.isLoggedIn = true;
        this.accountId = value.id;
        sessionStorage.setItem('accountId', value.id.toString());
        this.isAdmin = value.isAdmin;
        sessionStorage.setItem('isAdmin', value.isAdmin.toString());
      })
      .catch((reason: AxiosResponse) => {
        this.isLoginOrPasswordIncorrect = true;

        this.loaderStore.removeTask(LoginStore.LOGIN_ATTEMPT_TASK);
      });
  }

  @action
  reset() {
    this.isLoginOrPasswordIncorrect = false;

    this.loaderStore.removeTask(LoginStore.LOGIN_ATTEMPT_TASK);
  }

  @action
  logout() {
    this.isLoggedIn = false;
    sessionStorage.removeItem('accountId');
    sessionStorage.setItem('isLoggedIn', 'no');
    this.isAdmin = false;
    sessionStorage.setItem('isAdmin', 'false');
  }
}