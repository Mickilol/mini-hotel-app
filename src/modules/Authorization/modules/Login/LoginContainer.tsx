import * as React from 'react';
import ModalBase from '../../../Shared/components/Modal/ModalBase';
import Input from '../../../Shared/components/Inputs/Input';
import Button from '../../../Shared/components/Buttons/Button';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { observer } from 'mobx-react';
import { ILoginPayload, LoginStore } from './LoginStore';
import { lazyInject } from '../../../IoC';
import { IOnChangeProps } from '../../../Shared/types/IChangeProps';


interface ILoginContainerState {
  login: string;
  password: string;
}

@observer
class LoginContainer extends React.Component<RouteComponentProps, ILoginContainerState> {
  @lazyInject(LoginStore)
  readonly loginStore: LoginStore;

  constructor(props) {
    super(props);

    this.state = {
      login: '',
      password: '',
    };
  }

  componentWillUnmount() {
    this.loginStore.reset();
  }

  handleChange = ({name, value}: IOnChangeProps) => {
    this.setState({[name]: value} as any);
    this.loginStore.reset();
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const { login, password } = this.state;
    const payload = { login, password } as ILoginPayload;

    this.loginStore.loginAttempt(payload);
  };

  handleClose = () => {
    this.props.history.push('/');
  };

  render() {
    const { isLoginOrPasswordIncorrect } = this.loginStore;

    return (
      <ModalBase isOpen disableOverlay className='modal__login' onRequestClose={this.handleClose} hideCloseIcon={true}>
        <h2 className='modal__title'>
          Авторизация
        </h2>
        <form onSubmit={this.handleSubmit}>
          <Input
            name='login'
            label='Логин'
            placeholder='Введите ваш логин'
            errorMessage='Некорректный логин или пароль'

            onChange={this.handleChange}
            showError={isLoginOrPasswordIncorrect}
          />

          <Input
            name='password'
            type='password'
            label='Пароль'
            placeholder='Введите ваш пароль'
            errorMessage='Некорректный логин или пароль'

            onChange={this.handleChange}
            showError={isLoginOrPasswordIncorrect}
          />

          <Button className='dashboard-btn dashboard-btn--modal' type='submit'>
            Авторизоваться
          </Button>
        </form>


        <p className='modal__login__sign-up no-margin text-center'>
          Нет аккаунта?
          <br/>
          <Link to='/authorization/register'>
            <span className='clickable-label'>Зарегистрироваться!</span>
          </Link>
        </p>
      </ModalBase>
    );
  }

}

export default withRouter(LoginContainer);