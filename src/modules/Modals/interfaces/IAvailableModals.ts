import { ComponentClass } from 'react';

export interface IAvailableModals {
  'SUCCESS': ComponentClass<any>;
  'APARTMENT': ComponentClass<any>;
  'ADMIN_APARTMENT': ComponentClass<any>;
  'ADD_STAFF': ComponentClass<any>;
  'EDIT_STAFF': ComponentClass<any>;
}