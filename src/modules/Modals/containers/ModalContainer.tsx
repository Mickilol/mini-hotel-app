import './style.scss';
import * as React from 'react';
import { observer } from 'mobx-react';
import { lazyInject } from '../../IoC';
import { ModalStore } from '../store/ModalStore';
import { IAvailableModals } from '../interfaces/IAvailableModals';
import ModalSuccess from '../components/Success';
import ApartmentModal from '../../Dashboard/components/modals/ApartmentModal';
import AdminApartmentModal from '../../Dashboard/components/modals/AdminApartmentModal';
import AddStaffModal from '../../Dashboard/components/modals/AddStaffModal';
import EditStaffModal from '../../Dashboard/components/modals/EditStaffModal';

@observer
class ModalContainer extends React.Component {
  @lazyInject(ModalStore)
  private readonly modalStore: ModalStore;

  private readonly modals: IAvailableModals = {
    SUCCESS: ModalSuccess,
    APARTMENT: ApartmentModal,
    ADMIN_APARTMENT: AdminApartmentModal,
    ADD_STAFF: AddStaffModal,
    EDIT_STAFF: EditStaffModal,
  };

  render() {
    return this.modalStore.openedModals.map((modal, i) => {
      const Modal = this.modals[modal.name];
      return (
        <Modal
          key={i}
          onClose={() => this.modalStore.closeModal(modal.name)}
          {...modal.props}
        />
      );
    });
  }
}

export default ModalContainer;