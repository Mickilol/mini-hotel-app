import './style.scss';
import React from 'react';
import cn from 'classnames';
import { IOnChangeProps } from '../../../types/IChangeProps';

interface IRadioBtnProps {
  name: string;
  value?: string;
  label?: string;
  onChange?: (changeProps: IOnChangeProps) => void;
  checked?: boolean;
  showError?: boolean;
  disabled?: boolean;
  classNames?: string;
}

export default class RadioButton extends React.Component<IRadioBtnProps> {

  handleChange = ({ target: { name, value }}) => {
    if (this.props.onChange) {
      this.props.onChange({ name, value });
    }
  };

  render() {
    const {label, onChange, checked, showError, classNames, ...props} = this.props;

    return (
      <div className={cn('radio-btn', this.props.classNames)}>
        <label className={cn('radio-btn__container', {'error': showError})}><span>{label}</span>
          <input
            type='radio'
            checked={checked}
            onChange={this.handleChange}
            {...props}
          />
          <span className='radio-btn__check-mark'/>
        </label>
      </div>
    );
  }
}