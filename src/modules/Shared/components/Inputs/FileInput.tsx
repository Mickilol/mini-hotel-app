import React, { Component } from 'react';
import _ from 'lodash';
import classNames from 'classnames';
import Dropzone from 'react-dropzone';
import ImageFile from 'react-dropzone';

export interface IFileInputChangeProps {
  name: string;
  value: ImageFile;
}

interface IState {
  fileName: string;
  errorMessage: null | string;
}

interface IProps {
  name: string;
  onChange: (changeProps: IFileInputChangeProps) => void;
  label?: string;
  className?: string;
  placeholder?: string;
  errorMessage?: string;
  value?: ImageFile;
}

export default class FileInput extends Component<IProps, IState> {
  state = {fileName: '', errorMessage: null};

  constructor(props) {
    super(props);
    this.state.fileName = _.get(props, 'value.name');
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ errorMessage: nextProps.errorMessage });
  }

  onDropAccepted = files => {
    this.setState({fileName: files[0].name, errorMessage: null});
    _.invoke(this.props, 'onChange', {name: this.props.name, value: files[0]});
  };

  onDropRejected = files => {
    let errorMessage;
    if (files[0].size > 2e6) {
      errorMessage = 'File too large';
    }
    else {
      errorMessage = 'Incorrect file type';
    }
    this.setState({fileName: '', errorMessage});
  };

  render() {
    const {label, className, placeholder} = this.props;
    const {errorMessage, fileName} = this.state;
    const cn = classNames('form__field', {'form__show-error': !!errorMessage}, className);

    return (
      <div className={cn}>
        <label className='form__label'>{label}</label>
        <Dropzone
          accept='image/png,image/gif,image/jpg,image/jpeg'
          className='form__input form__input-file'
          multiple={false}
          maxSize={2e6}
          onDropAccepted={this.onDropAccepted}
          onDropRejected={this.onDropRejected}
        >
          <span className={classNames({'placeholder': !fileName}, 'header_description')}>
            {fileName || placeholder}
            </span>
          <span style={{textDecoration: 'underline'}}>CHOOSE FILE</span>
        </Dropzone>
        <span className='form__error-message'>{errorMessage}</span>
      </div>
    );
  }
}