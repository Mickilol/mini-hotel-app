import './style.scss';
import React from 'react';
import cn from 'classnames';

interface ICheckboxProps {
  name: string;
  label: string;
  onChange: any;
  className?: string;
  checked?: boolean;
  showError?: boolean;
}

export default class CheckBox extends React.Component<ICheckboxProps> {

  render() {
    const {label, onChange, checked, showError, className, ...props} = this.props;

    return (
      <div className={'checkbox ' + className}>
        <label className={cn('checkbox__container', {'error': showError})}><span>{label}</span>
          <input
            type='checkbox'
            checked={checked}
            onChange={({target: {name, checked}}) => onChange({name, value: checked})}
            {...props}
          />
          <span className='checkbox__check-mark'/>
        </label>
      </div>
    );
  }
}