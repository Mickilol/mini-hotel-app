import './style.scss';
import * as React from 'react';
import classNames from 'classnames';
import InputMask from 'react-input-mask';
import * as _ from 'lodash';
import { InputHTMLAttributes } from 'react';


interface IInputProps extends InputHTMLAttributes<HTMLInputElement> {
  label?: string;
  action?: object;
  showError?: boolean;
  errorMessage?: string;
  mask?: string;
  maskChar?: string | null;
  onChange?: (props) => void;
}

export default class Input extends React.Component<IInputProps> {

  public handleChange = ({target: {value}}) => {
    _.invoke(this.props, 'onChange', { ...this.props, value });
  };

  public render() {
    const {label, className, onChange, action, showError, errorMessage, ...props} = this.props;
    const cn = classNames(
      'form__field',
      {'form__show-error': showError},
      className,
    );
    const isHasLabel = !!label;

    return (
      <div className={cn} hidden={this.props.hidden}>
        {label && label.length > 0 ? <label className='form__label'>{label}</label> : null }
        <div className={classNames('form__input', {'disabled': props.disabled})}>
          <InputMask {...props} onChange={this.handleChange} />
          {action}
        </div>
        <span className={classNames('form__error-message', {'form__error-message--low': !isHasLabel})}>
          {errorMessage}
        </span>
      </div>
    );
  }
}