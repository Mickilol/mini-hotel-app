import * as React from 'react';
import ReactSelect from 'react-select';
import classNames from 'classnames';
import './style.scss';

interface ISelectSearchProps {
  className?: string;
  label?: string;
  name?: string;
  onChange?: (ISelectInputProps) => void;
  options?: Array<{ label: string, value: any }>;
  placeholder?: string;
  errorMessage?: string;
  showError?: boolean;
  value?: any;
}

interface ISearchSelectState {
  selectedOption: string | null;
}

export default class SelectSearch extends React.Component<ISelectSearchProps, ISearchSelectState> {
  state = { selectedOption: null };

  constructor(props) {
    super(props);

    if (this.props.value && this.props.options) {
      const selectedOption = this.props.options.find(option => option.value === props.value);
      if (selectedOption) {
        // @ts-ignore
        this.state.selectedOption = selectedOption.value;
      }
    }
  }

  handleChange = (selectedOption) => {
    this.setState({ selectedOption });
    const value = selectedOption ? selectedOption.value : null;
    this.props.onChange && this.props.onChange({...this.props, value});
  };

  render() {
    const {label, className, options, placeholder, showError, errorMessage } = this.props;
    const { selectedOption } = this.state;

    return (
      <div className={classNames('form__field', {'form__show-error': showError}, className)}>
        {label && label.length > 0 ? <label className='form__label'>{label}</label> : null }

        <ReactSelect
          value={selectedOption}
          onChange={this.handleChange}
          options={options}
          isSearchable
          placeholder={placeholder}
          className='react-select-container'
        />
        <span className='form__error-message'>{errorMessage}</span>
      </div>
    );
  }
}