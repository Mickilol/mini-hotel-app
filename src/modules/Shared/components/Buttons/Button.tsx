import * as React from 'react';
import classNames from 'classnames';
import './style.scss';

interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  onClick?: any;
  className?: string;
  name?: 'default' | 'transparent' | 'white' | 'dark' | 'sell' | 'currency' | 'modal' | 'fluid' | 'delete' | 'submit';
}

export default class Button extends React.Component<IButtonProps> {
  public render() {
    const { className, children, name, ...props} = this.props;
    return(
      <button className={classNames(className, 'dashboard-btn', `dashboard-btn--${name}`)} {...props}>
        {children}
      </button>
    );
  }
}