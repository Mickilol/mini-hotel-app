import 'reflect-metadata';

import { Container } from 'inversify';
import { dashboardSingletons } from './Dashboard/DashboardIoC';
import { LoaderStore } from './Shared/modules/Loader/store/LoaderStore';
import { ModalStore } from './Modals/store/ModalStore';
import { authorizationSingletons } from './Authorization/AuthorizationIoC';
import { AxiosWrapper } from './Shared/services/AxiosWrapper';

const container = new Container();

const singletons = [
  AxiosWrapper,
  LoaderStore,
  ModalStore,

  ...authorizationSingletons,
  ...dashboardSingletons,
];

for (const singleton of singletons) {
  container.bind<any>(singleton).to(singleton).inSingletonScope();
}

// New decorator syntax
const lazyInject = (k: any) => (proto: any, key: string, descriptor?: any) => {
  descriptor.initializer = () => container.get(k);
};

export {
  container,
  lazyInject,
};