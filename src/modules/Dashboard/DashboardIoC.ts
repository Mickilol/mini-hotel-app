import { DashboardStore } from './stores/DashboardStore';
import { BookkeepingStore } from './stores/BookkeepingStore';


export const dashboardSingletons = [
  DashboardStore,
  BookkeepingStore,
];