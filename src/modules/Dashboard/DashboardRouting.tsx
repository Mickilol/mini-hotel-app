import * as React from 'react';
import { Redirect, Route, Switch } from 'react-router';
import DashboardContainer from './containers/DashboardContainer';
import { BookkeepingContainer } from './containers/BookkeepingContainer';

class DashboardRouting extends React.Component{

  render() {
    return (
      <Switch>

        <Route path='/bookkeeping' component={BookkeepingContainer}/>
        <Route path='/' component={DashboardContainer}/>

        <Redirect to='/'/>
      </Switch>
    );
  }
}

export default DashboardRouting;