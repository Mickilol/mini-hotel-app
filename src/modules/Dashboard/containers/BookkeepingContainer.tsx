import * as React from 'react';
import { lazyInject } from '../../IoC';
import { DashboardStore } from '../stores/DashboardStore';
import { Header } from '../components/Header';
import { Footer } from '../components/Footer';
import Select from '../../Shared/components/Inputs/Select';
import Input from '../../Shared/components/Inputs/Input';
import Button from '../../Shared/components/Buttons/Button';
import {
  BookkeepingStore,
  IAddPaymnetPayload,
  IApartExpenseAmountPayload,
  ISumExpensesPayload,
  periodArray,
} from '../stores/BookkeepingStore';
import { observer } from 'mobx-react';
import InputDate from '../../Shared/components/Inputs/InputDate';
import moment from 'moment';
import { ModalStore } from '../../Modals/store/ModalStore';


interface IState {
  apartmentId: number;
  period: string;
  paymentYear: string;

  periodSum: string;
  paymentYearSum: string;

  paymentApartmentId: number;
  expenseId: number;
  paymentDate: string;
  paymentAmount: number;
  paymentPeriod: string;
}

@observer
export class BookkeepingContainer extends React.Component<{}, IState> {

  @lazyInject(DashboardStore)
  private readonly dashboardStore: DashboardStore;

  @lazyInject(BookkeepingStore)
  private readonly store: BookkeepingStore;

  @lazyInject(ModalStore)
  private readonly modalStore: ModalStore;

  constructor(props) {
    super(props);

    this.state = {
      apartmentId: -1,
      period: '',
      paymentYear: '',

      periodSum: '',
      paymentYearSum: '',

      paymentApartmentId: -1,
      expenseId: -1,
      paymentDate: '',
      paymentAmount: 0,
      paymentPeriod: '',
    };
  }

  componentWillMount() {
    this.dashboardStore.fetchApartments();
    this.dashboardStore.fetchApartmentsFacilities();
    this.store.fetchExpensesList();
    this.store.fetchStaffList();
  }

  handleChange = ({name, value}) => {
    this.setState({[name]: value} as any);
  };

  handleSumExpenses = async () => {
    const period = this.state.periodSum;
    const paymentYear = this.state.paymentYearSum;

    const payload = {
      period,
      paymentYear,
    } as ISumExpensesPayload;

    await this.store.fetchSumExpenses(payload);
  };

  handleApartExpense = async () => {
    const {
      apartmentId,
      period,
      paymentYear,
    } = this.state;

    const payload = {
      apartmentId,
      period,
      paymentYear,
    } as IApartExpenseAmountPayload;

    await this.store.fetchApartmentExpenseAmount(payload);
  };

  handleAddPayment = async () => {

    const payload = {
      apartmentId: this.state.paymentApartmentId,
      expenseId: this.state.expenseId,
      paymentDate: moment(this.state.paymentDate).format('YYYY-MM-DD'),
      price: Number(this.state.paymentAmount),
      period: this.state.paymentPeriod,
    } as IAddPaymnetPayload;

    await this.store.addExpense(payload);
  };

  handleAddStaff = () => {
    this.modalStore.openModal('ADD_STAFF');
  };

  handleEditStaff = (element) => {
    this.modalStore.openModal('EDIT_STAFF', {...element});
  };

  render() {
    const apartments = this.dashboardStore.apartmentsArray;
    const expenses = this.store.expensesArray;
    const staffList = this.store.staffList;
    const { apartmentExpenseAmount, sumExpenses } = this.store;

    return (
      <div className='mini-hotel row'>
        <Header />
        <h1 className='header-1 col-12 text-center'>Отчетность</h1>

        <h2 className='col-12 mb-3 header-2'>
          Расходы квартиры за период
        </h2>

        <div className='row mb-5' style={{width: '100%'}}>
          <div className='col-4'>
            <Select
              label='Квартира'
              name='apartmentId'
              options={apartments.map(element => ({ value: element.offer.id, label: element.offer.address }))}
              onChange={this.handleChange}
            />
          </div>

          <div className='col-4'>
            <Select
              label='Период'
              name='period'
              options={periodArray.map(element => ({ value: element, label: element }))}
              onChange={this.handleChange}
            />
          </div>

          <div className='col-4'>
            <Input
              label='Год'
              name='paymentYear'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-2'>
            <Button name='white' onClick={this.handleApartExpense}>
              Узнать
            </Button>
          </div>

          <div className='col-6 d-flex align-items-center'>
            <span className='result'>Результат: {apartmentExpenseAmount}</span>
          </div>
        </div>

        <h2 className='col-12 mb-3 header-2'>
          Сумма расходов за период
        </h2>

        <div className='row mb-5' style={{width: '100%'}}>

          <div className='col-6'>
            <Select
              label='Период'
              name='periodSum'
              options={periodArray.map(element => ({ value: element, label: element }))}
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <Input
              label='Год'
              name='paymentYearSum'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-2'>
            <Button name='white' onClick={this.handleSumExpenses}>
              Узнать
            </Button>
          </div>

          <div className='col-6 d-flex align-items-center'>
            <span className='result'>Результат: {sumExpenses}</span>
          </div>
        </div>

        <h2 className='col-12 mb-3 header-2'>
          Добавить платеж
        </h2>

        <div className='row' style={{width: '100%', marginBottom: '100px'}}>
          <div className='col-6'>
            <Select
              label='Квартира'
              name='paymentApartmentId'
              options={apartments.map(element => ({ value: element.offer.id, label: element.offer.address }))}
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <Select
              label='Предмет платежа'
              name='expenseId'
              options={expenses.map(element => ({ value: element.id, label: element.name }))}
              onChange={this.handleChange}
            />
          </div>

          <div className='col-4'>
            <InputDate
              name='paymentDate'
              onChange={this.handleChange}
              label='Дата платежа'
            />
          </div>

          <div className='col-4'>
            <Input
              label='Размер платежа (руб.)'
              name='paymentAmount'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-4'>
            <Select
              label='Период'
              name='paymentPeriod'
              options={periodArray.map(element => ({ value: element, label: element }))}
              onChange={this.handleChange}
            />
          </div>

          <div className='col-2'>
            <Button name='white' onClick={this.handleAddPayment}>
              Добавить
            </Button>
          </div>
        </div>

        <h2 className='col-12 mb-3 header-2'>
          Персонал
        </h2>

        <div className='staff-list mb-5'>
          <div className='row staff-list__header'>
            <div className='col-4 text--center staff-list__header__item'>
              ФИО
            </div>
            <div className='col-2 text--center staff-list__header__item'>
              Должность
            </div>
            <div className='col-2 text--center staff-list__header__item'>
              Зарплата
            </div>
            <div className='col-2 text--center staff-list__header__item'>
              Паспортные данные
            </div>
            <div className='col-2 text--center staff-list__header__item'>
              Телефон
            </div>
          </div>
          {staffList.map(element => (
            <div className='row staff-list__body' key={element.passportData}
                 onClick={() => this.handleEditStaff(element)}
            >
              <div className='col-4 text--center staff-list__body__item'>
                {`${element.lastName} ${element.name}  ${element.secondName}`}
              </div>
              <div className='col-2 text--center staff-list__body__item'>
                {element.position}
              </div>
              <div className='col-2 text--center staff-list__body__item'>
                {element.salary}
              </div>
              <div className='col-2 text--center staff-list__body__item'>
                {element.passportData}
              </div>
              <div className='col-2 text--center staff-list__body__item'>
                {element.phone}
              </div>
            </div>
          ))}

          <Button className='mt-3' name='white' onClick={this.handleAddStaff}>Добавить</Button>
        </div>

        <Footer />
      </div>
    );
  }

}