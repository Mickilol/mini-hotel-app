import * as React from 'react';
import { lazyInject } from '../../IoC';
import {
  DashboardStore,
  IAddApartPayload,
  IAddRentPayload,
  IAddReviewPayload,
  IUpdateFacilitiesPayload
} from '../stores/DashboardStore';
import { observer } from 'mobx-react';
import { ModalStore } from '../../Modals/store/ModalStore';
import { LoginStore } from '../../Authorization/modules/Login/LoginStore';
import '../styles/style.scss';

import MainPinImage from '../../Shared/assets/images/main-pin-image.png';
import Draggable from 'react-draggable';
import { ApartmentAddition } from '../components/ApartmentAddition';
import { Footer } from '../components/Footer';
import { Rent } from '../components/Rent';
import { Header } from '../components/Header';
import Reviews from '../components/Reviews';


interface IState {
  ref: any;
  currentCoords: {
    x: number;
    y: number;
  };
}

@observer
class DashboardContainer extends React.Component<{}, IState> {
  @lazyInject(ModalStore)
  private readonly modalStore: ModalStore;

  @lazyInject(DashboardStore)
  private readonly store: DashboardStore;

  @lazyInject(LoginStore)
  private readonly loginStore: LoginStore;

  constructor(props) {
    super(props);

    this.state = {
      ref: React.createRef(),
      currentCoords: {
        x: 0,
        y: 0,
      },
    };
  }

  componentWillMount() {
    this.store.fetchApartments();
    this.store.fetchApartmentsFacilities();
    this.store.fetchReviewsList();
  }

  handleClick = (data) => {
    const facilities = this.store.apartmentsFacilities.filter(element =>
      element.apartmentId === data.offer.id,
    );

    if (this.loginStore.isAdmin) {
      this.modalStore.openModal('ADMIN_APARTMENT', {data, facilities});
      return;
    }

    this.modalStore.openModal('APARTMENT', {data, facilities});
  };

  handleDrag = (evt) => {
    const ref = document.querySelector('.map__pin--main').getBoundingClientRect();

    this.setState({
      currentCoords: {
        x: ref.left - 206.5,
        y: ref.top + 35,
      },
    });
  };
  
  handleApartmentAdding = async (payload: IAddApartPayload, facilities: string[]) => {
    await this.store.addApartment(payload);
    await this.store.fetchApartments();

    const apartment = this.store.apartmentsArray[this.store.apartmentsArray.length - 1];

    facilities.forEach(item => {
      const payload = {
        apartmentId: apartment.offer.id,
        facilitiesName: item,
        value: '',
      } as IUpdateFacilitiesPayload;

      this.store.updateApartmentsFacilities(payload);
    });
  };

  handleRent = (payload: IAddRentPayload) => {
     this.store.addRent(payload);
  };

  handleAddReview = async (payload: IAddReviewPayload) => {
    await this.store.addReview(payload);
  };

  render() {
    const apartments = this.store.apartmentsArray;
    const isAdmin = this.loginStore.isAdmin;

    return (
      <div className='mini-hotel' >
        <Header />

        <section className='map'>
          <div className='map__pins'>
            { isAdmin
              ? apartments.map((element) => (
                  !element.isDeleted &&
                  <button onClick={() => this.handleClick(element)}
                          style={{left: `${element.location.x}px`, top: `${element.location.y}px`}}
                          key={element.offer.id} className='map__pin'>
                    <img src={element.preview.photo} width='40' height='40' draggable={false} />
                  </button>
                ),
              )
              : apartments.map((element) => (
                  !element.isDeleted && element.offer.status === 'свободна' &&
                  <button onClick={() => this.handleClick(element)}
                    style={{left: `${element.location.x}px`, top: `${element.location.y}px`}}
                    key={element.offer.id} className='map__pin'>
                    <img src={element.preview.photo} width='40' height='40' draggable={false} />
                  </button>
                  ),
                )
            }

            <div className='map__pinsoverlay'><h2>И снова Токио!</h2></div>

            <Draggable
              onDrag={this.handleDrag}
              bounds={{left: -600, top: -290, right: 540, bottom: 300}}
            >
              <button className='map__pin map__pin--main' ref={this.state.ref} hidden={!isAdmin}>
                <img src={MainPinImage} width='44' height='44' draggable={false}/>
                  <svg viewBox='0 0 70 70'>
                    <defs>
                      <path d='M35,35m-23,0a23,23 0 1,1 46,0a23,23 0 1,1 -46,0' id='tophalf'/>
                    </defs>

                    <ellipse cx='35' cy='35' rx='35' ry='35' fill='rgba(255, 86, 53, 0.7)'/>
                    <text>
                      Поставь меня куда-нибудь
                    </text>
                  </svg>
              </button>
            </Draggable>
          </div>
        </section>

        {isAdmin
          ? <ApartmentAddition
              currentCoords={this.state.currentCoords}
              onApartmentAdding={this.handleApartmentAdding}
            />
          : <Rent
            apartments={apartments.filter(element => (
                element.isDeleted !== true && element.offer.status === 'свободна'
              ))}
            onAddRent={this.handleRent}
          />
        }

        <Reviews
          reviews={this.store.reviews}
          apartments={apartments.filter(element => (
            element.isDeleted !== true
          ))}
          userId={Number(this.loginStore.accountId)}
          onAddReview={this.handleAddReview}
        />

        <Footer />
      </div>
    );
  }
}

export default DashboardContainer;