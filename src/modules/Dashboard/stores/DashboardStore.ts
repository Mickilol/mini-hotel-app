import { inject, injectable } from 'inversify';
import { action, observable } from 'mobx';
import { AxiosWrapper } from '../../Shared/services/AxiosWrapper';
import { LoaderStore } from '../../Shared/modules/Loader/store/LoaderStore';
import { AxiosResponse } from 'axios';


export enum Facilities {
  'wifi' = 'wifi',
  'dishwasher' = 'кухня',
  'parking' = 'парковка',
  'washer' = 'стиралка',
  'elevator' = 'лифт',
  'conditioner' = 'кондиционер',
}

export interface IAddApartPayload {
  roomNumber: string;
  square: number;
  floor: number;
  costPerDay: number;
  address: string;
  description: string;
  status: string;
  apartmentNumber: string;
  x: number;
  y: number;
  preview: string;
}

export interface IUpdateApartPayload {
  id: number;
  roomNumber: string;
  square: number;
  floor: number;
  costPerDay: number;
  address: string;
  description: string;
  status: string;
  apartmentNumber: string;
  x: number;
  y: number;
  preview: string;
}

export interface IDeleteApartPayload {
  id: number;
}

export interface IUpdateFacilitiesPayload {
  apartmentId: number;
  facilitiesName: string;
  value: string;
}

export interface IDeleteFacilitiesPayload {
  apartmentId: number;
  facilitiesName: string;
}

export interface IAddRentPayload {
  rentDate: string;
  apartmentId: number;
  userId: number;
  term: number;
  price: number;
}

export interface IAddReviewPayload {
  userId: number;
  apartmentId: number;
  createDate: string;
  message: string;
  score: number;
}

@injectable()
export class DashboardStore {
  protected static readonly FETCH_APARTMENT_TASK = 'FETCH_APARTMENT_TASK';
  protected static readonly ADDING_APARTMENT_TASK = 'ADDING_APARTMENT_TASK';
  protected static readonly UPDATE_APARTMENT_TASK = 'UPDATE_APARTMENT_TASK';
  protected static readonly DELETE_APARTMENT_TASK = 'DELETE_APARTMENT_TASK';
  protected static readonly FETCH_APARTMENT_FACILITIES_TASK = 'FETCH_APARTMENT_FACILITIES_TASK';
  protected static readonly UPDATE_APARTMENT_FACILITIES_TASK = 'UPDATE_APARTMENT_FACILITIES_TASK';
  protected static readonly DELETE_APARTMENT_FACILITIES_TASK = 'DELETE_APARTMENT_FACILITIES_TASK';
  protected static readonly ADDING_RENT_TASK = 'ADDING_RENT_TASK';
  protected static readonly FETCH_REVIEWS_LIST_TASK = 'FETCH_REVIEWS_LIST_TASK';
  protected static readonly ADD_REVIEW_TASK = 'ADD_REVIEW_TASK';

  @inject(AxiosWrapper)
  private readonly axiosWrapper: AxiosWrapper;

  @inject(LoaderStore)
  private readonly loaderStore: LoaderStore;

  @observable apartmentsArray: any[] = [];
  @observable apartmentsFacilities: any[] = [];
  @observable reviews: any[] = [];

  @action
  fetchApartments = async () => {
    this.loaderStore.addTask(DashboardStore.FETCH_APARTMENT_TASK);

    await this.axiosWrapper.get('/dashboard/get-apartments/')
      .then(value => {
        this.apartmentsArray = value;

        this.loaderStore.removeTask(DashboardStore.FETCH_APARTMENT_TASK);
      })
      .catch((reason: AxiosResponse) => {
        this.loaderStore.removeTask(DashboardStore.FETCH_APARTMENT_TASK);
      });
  };

  @action
  addApartment = async (payload: IAddApartPayload) => {
    this.loaderStore.addTask(DashboardStore.ADDING_APARTMENT_TASK);

    await this.axiosWrapper.post('/admin/add-apartment/', payload)
      .then(() => {
        this.fetchApartments();

        this.loaderStore.removeTask(DashboardStore.ADDING_APARTMENT_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(DashboardStore.ADDING_APARTMENT_TASK);
      });
  };

  @action
  updateApartment(payload: IUpdateApartPayload) {
    this.loaderStore.addTask(DashboardStore.UPDATE_APARTMENT_TASK);

    return new Promise(() => {
      this.axiosWrapper.put('/admin/update-apartment/', payload)
        .then(async () => {
          await this.fetchApartments();

          this.loaderStore.removeTask(DashboardStore.UPDATE_APARTMENT_TASK);
        })
        .catch((reason: AxiosResponse) => {
          this.loaderStore.removeTask(DashboardStore.UPDATE_APARTMENT_TASK);
        });
    });

  }

  @action
  deleteApartment(payload: IDeleteApartPayload) {
    this.loaderStore.addTask(DashboardStore.DELETE_APARTMENT_TASK);

    this.axiosWrapper.delete('/admin/delete-apartment/', payload)
      .then(() => {
        this.fetchApartments();

        this.loaderStore.removeTask(DashboardStore.DELETE_APARTMENT_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(DashboardStore.DELETE_APARTMENT_TASK);
      });
  }

  @action
  fetchApartmentsFacilities() {
    this.loaderStore.addTask(DashboardStore.FETCH_APARTMENT_FACILITIES_TASK);

    this.axiosWrapper.get('/dashboard/get-apartment-facilities/')
      .then((value) => {
        this.apartmentsFacilities = value;

        this.loaderStore.removeTask(DashboardStore.FETCH_APARTMENT_FACILITIES_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(DashboardStore.FETCH_APARTMENT_FACILITIES_TASK);
      });
  }
  
  @action
  updateApartmentsFacilities(payload: IUpdateFacilitiesPayload) {
    this.loaderStore.addTask(DashboardStore.UPDATE_APARTMENT_FACILITIES_TASK);

    this.axiosWrapper.post('/admin/add-apartment-facilities/', payload)
      .then(() => {
        this.fetchApartmentsFacilities();

        this.loaderStore.removeTask(DashboardStore.UPDATE_APARTMENT_FACILITIES_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(DashboardStore.UPDATE_APARTMENT_FACILITIES_TASK);
      });
  }

  @action
  deleteApartmentFacilities = async (payload: IDeleteFacilitiesPayload) => {
    this.loaderStore.addTask(DashboardStore.DELETE_APARTMENT_FACILITIES_TASK);

    await this.axiosWrapper.delete('/admin/delete-apartment-facilities/', payload)
      .then(() => {
        this.fetchApartmentsFacilities();

        this.loaderStore.removeTask(DashboardStore.DELETE_APARTMENT_FACILITIES_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(DashboardStore.DELETE_APARTMENT_FACILITIES_TASK);
      });
  };

  @action
  async addRent(payload: IAddRentPayload) {
    this.loaderStore.addTask(DashboardStore.ADDING_RENT_TASK);

    await this.axiosWrapper.post('/admin/add-rent/', payload)
      .then( () => {
        const apartment = this.apartmentsArray.find(element => element.offer.id === payload.apartmentId);
        apartment.offer.status = 'занята';

        let {
          costPerDay,
          square,
          floor,
        } = apartment.offer;

        let {
          x,
          y,
        } = apartment.location;

        costPerDay = Number(costPerDay);
        square = Number(square);
        floor = Number(floor);
        x = Number(x);
        y = Number(y);

        const ApartPayload = {
          id: apartment.offer.id,
          roomNumber: apartment.offer.roomNumber,
          square,
          floor,
          costPerDay,
          address: apartment.offer.address,
          description: apartment.offer.description,
          status: apartment.offer.status,
          apartmentNumber: apartment.offer.apartmentNumber,
          x,
          y,
          preview: apartment.preview.photo,
        } as IUpdateApartPayload;

        this.updateApartment(ApartPayload);

        this.loaderStore.removeTask(DashboardStore.ADDING_RENT_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(DashboardStore.ADDING_RENT_TASK);
      });
  }

  @action
  async fetchReviewsList() {
    this.loaderStore.addTask(DashboardStore.FETCH_REVIEWS_LIST_TASK);

    await this.axiosWrapper.get('/dashboard/get-review-list/')
      .then((value) => {
        this.reviews = value;

        this.loaderStore.removeTask(DashboardStore.FETCH_REVIEWS_LIST_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(DashboardStore.FETCH_REVIEWS_LIST_TASK);
      });
  }


  @action
  addReview = async (payload: IAddReviewPayload) => {
    this.loaderStore.addTask(DashboardStore.ADD_REVIEW_TASK);

    await this.axiosWrapper.post('/account/add-review/', payload)
      .then(() => {
        this.fetchReviewsList();

        this.loaderStore.removeTask(DashboardStore.ADD_REVIEW_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(DashboardStore.ADD_REVIEW_TASK);
      });
  };
}