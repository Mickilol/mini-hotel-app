import { inject, injectable } from 'inversify';
import { action, observable } from 'mobx';
import { AxiosWrapper } from '../../Shared/services/AxiosWrapper';
import { LoaderStore } from '../../Shared/modules/Loader/store/LoaderStore';
import { AxiosResponse } from 'axios';


export interface IApartExpenseAmountPayload {
  apartmentId: number;
  period: string;
  paymentYear: string;
}

export interface ISumExpensesPayload {
  period: string;
  paymentYear: string;
}

export interface IAddPaymnetPayload {
  apartmentId: number;
  expenseId: number;
  paymentDate: string;
  price: number;
  period: string;
}

export interface IAddStaffPayload {
  lastName: string;
  name: string;
  secondName: string;
  position: string;
  salary: number;
  passportData: string;
  phone: string;
}

export interface IUpdateStaffPayload {
  id: number;
  lastName: string;
  name: string;
  secondName: string;
  position: string;
  salary: number;
  passportData: string;
  phone: string;
}

export interface IDeleteStaff {
  id: number;
}

export const periodArray = [
  'январь',
  'февраль',
  'март',
  'апрель',
  'май',
  'июнь',
  'июль',
  'август',
  'сентябрь',
  'октябрь',
  'ноябрь',
  'декабрь',
  '1 квартал',
  '2 квартал',
  '3 квартал',
  '4 квартал',
];

@injectable()
export class BookkeepingStore {
  protected static readonly FETCH_APARTMENT_EXPENSE_AMOUNT_TASK = 'FETCH_APARTMENT_EXPENSE_AMOUNT_TASK';
  protected static readonly FETCH_SUM_EXPENSE_TASK = 'FETCH_SUM_EXPENSE_TASK';
  protected static readonly FETCH_EXPENSES_LIST_TASK = 'FETCH_EXPENSES_LIST_TASK';
  protected static readonly ADD_EXPENSE_TASK = 'ADD_EXPENSE_TASK';
  protected static readonly FETCH_STAFF_TASK = 'FETCH_STAFF_TASK';
  protected static readonly ADD_STAFF_TASK = 'ADD_STAFF_TASK';
  protected static readonly UPDATE_STAFF_TASK = 'UPDATE_STAFF_TASK';
  protected static readonly DELETE_STAFF_TASK = 'DELETE_STAFF_TASK';

  @inject(AxiosWrapper)
  private readonly axiosWrapper: AxiosWrapper;

  @inject(LoaderStore)
  private readonly loaderStore: LoaderStore;

  @observable apartmentExpenseAmount = 0;
  @observable sumExpenses = 0;
  @observable expensesArray: any[] = [];
  @observable staffList: any[] = [];

  @action
  fetchApartmentExpenseAmount = async (payload: IApartExpenseAmountPayload) => {
    this.loaderStore.addTask(BookkeepingStore.FETCH_APARTMENT_EXPENSE_AMOUNT_TASK);

    await this.axiosWrapper.post('/admin/get-apartment-expense/', payload)
      .then(value => {
        this.apartmentExpenseAmount = value[0].sum;

        this.loaderStore.removeTask(BookkeepingStore.FETCH_APARTMENT_EXPENSE_AMOUNT_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(BookkeepingStore.FETCH_APARTMENT_EXPENSE_AMOUNT_TASK);
      });
  };

  @action
  fetchSumExpenses = async (payload: ISumExpensesPayload) => {
    this.loaderStore.addTask(BookkeepingStore.FETCH_SUM_EXPENSE_TASK);

    await this.axiosWrapper.post('/admin/get-expenses-sum-for-period/', payload)
      .then(value => {
        this.sumExpenses = value[0].sum;

        this.loaderStore.removeTask(BookkeepingStore.FETCH_SUM_EXPENSE_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(BookkeepingStore.FETCH_SUM_EXPENSE_TASK);
      });
  };

  @action
  fetchExpensesList = async () => {
    this.loaderStore.addTask(BookkeepingStore.FETCH_EXPENSES_LIST_TASK);

    await this.axiosWrapper.get('/admin/get-expenses-list/')
      .then((value) => {
        this.expensesArray = value;

        this.loaderStore.removeTask(BookkeepingStore.FETCH_EXPENSES_LIST_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(BookkeepingStore.FETCH_EXPENSES_LIST_TASK);
      });
  };

  @action
  addExpense = async (payload: IAddPaymnetPayload) => {
    this.loaderStore.addTask(BookkeepingStore.ADD_EXPENSE_TASK);

    await this.axiosWrapper.post('/admin/add-apartment-expenses/', payload)
      .then(() => {

        this.loaderStore.removeTask(BookkeepingStore.ADD_EXPENSE_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(BookkeepingStore.ADD_EXPENSE_TASK);
      });
  };

  @action
  fetchStaffList = async () => {
    this.loaderStore.addTask(BookkeepingStore.FETCH_STAFF_TASK);

    await this.axiosWrapper.get('/admin/get-staff-list/')
      .then((value) => {
        this.staffList = value;

        this.loaderStore.removeTask(BookkeepingStore.FETCH_STAFF_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(BookkeepingStore.FETCH_STAFF_TASK);
      });
  };

  @action
  addStaff = async (payload: IAddStaffPayload) => {
    this.loaderStore.addTask(BookkeepingStore.ADD_STAFF_TASK);

    await this.axiosWrapper.post('/admin/add-staff/', payload)
      .then(() => {
        this.fetchStaffList();

        this.loaderStore.removeTask(BookkeepingStore.ADD_STAFF_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(BookkeepingStore.ADD_STAFF_TASK);
      });
  };

  @action
  updateStaff = async (payload: IUpdateStaffPayload) => {
    this.loaderStore.addTask(BookkeepingStore.UPDATE_STAFF_TASK);

    await this.axiosWrapper.put('/admin/update-staff/', payload)
      .then(() => {
        this.fetchStaffList();

        this.loaderStore.removeTask(BookkeepingStore.UPDATE_STAFF_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(BookkeepingStore.UPDATE_STAFF_TASK);
      });
  };

  @action
  deleteStaff = async (payload: IDeleteStaff) => {
    this.loaderStore.addTask(BookkeepingStore.DELETE_STAFF_TASK);

    await this.axiosWrapper.delete('/admin/delete-staff/', payload)
      .then(() => {
        this.fetchStaffList();

        this.loaderStore.removeTask(BookkeepingStore.DELETE_STAFF_TASK);
      })
      .catch((reason: AxiosResponse) => {
        console.log(reason);
        this.loaderStore.removeTask(BookkeepingStore.DELETE_STAFF_TASK);
      });
  };


}