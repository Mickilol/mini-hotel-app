import * as React from 'react';
import Input from '../../Shared/components/Inputs/Input';
import TextArea from '../../Shared/components/Inputs/TextArea/TextArea';
import classNames from 'classnames';
import {
  Facilities, IAddApartPayload,
  } from '../stores/DashboardStore';

interface IProps {
  currentCoords: {
    x: number;
    y: number;
  };
  onApartmentAdding: (payload: IAddApartPayload, facilities: string[]) => void;
}

interface IState {
  preview: string;
  roomNumber: string;
  square: number;
  floor: number;
  costPerDay: number;
  address: string;
  description: string;
  status: string;
  apartmentNumber: string;

  wifi: boolean;
  dishwasher: boolean;
  parking: boolean;
  washer: boolean;
  elevator: boolean;
  conditioner: boolean;
}

export class ApartmentAddition extends React.Component<IProps, IState> {

  constructor(props) {
    super(props);

    this.state = {
      preview: 'muffin.png',
      roomNumber: '',
      square: 0,
      floor: 0,
      costPerDay: 0,
      address: '',
      description: '',
      status: 'свободна',
      apartmentNumber: '',

      wifi: false,
      dishwasher: false,
      parking: false,
      washer: false,
      elevator: false,
      conditioner: false,
    };
  }

  handleChange = ({ name, value }) => {
    this.setState({[name]: value} as IState);
  };

  handlePhotoChange = ({name, value}) => {
    const array = value.split('\\');
    const preview = array[array.length - 1];

    this.setState({ preview });
  };

  handleCheckboxChange = ({ target: {value} }) => {
    this.setState({[value]: !this.state[value]} as any);
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      roomNumber,
      address,
      description,
      status,
      apartmentNumber,
      preview,
    } = this.state;

    let {
      square,
      floor,
      costPerDay,
    } = this.state;

    let { x, y } = this.props.currentCoords;

    square = Number(square);
    floor = Number(floor);
    costPerDay = Number(costPerDay);
    x = Number(x) === 0 ? 631 : Number(x);
    y = Number(y) === 0 ? 250 : Number(y);

    const payload = {
      roomNumber,
      square,
      floor,
      costPerDay,
      address,
      description,
      status,
      apartmentNumber,
      x,
      y,
      preview,
    } as IAddApartPayload;

    // Обработка удобств на добавление

    const facilitiesArray = ['wifi', 'dishwasher', 'parking', 'washer', 'elevator', 'conditioner'];

    const facilities = {
      wifi: this.state.wifi,
      dishwasher: this.state.dishwasher,
      parking: this.state.parking,
      washer: this.state.washer,
      elevator: this.state.elevator,
      conditioner: this.state.conditioner,
    };

    const toAddFacilities = [];

    for (const item of facilitiesArray) {
      if (facilities[item] === true) {
        toAddFacilities.push(Facilities[item]);
      }
    }

    this.props.onApartmentAdding(payload, toAddFacilities);
  };

  render() {
    const x = this.props.currentCoords.x === 0 ? 631 : this.props.currentCoords.x;
    const y = this.props.currentCoords.y === 0 ? 205 : this.props.currentCoords.y;

    return (
      <div className='notice row'>
        <h2 className='notice__title'>Добавить апартаменты</h2>

        <form className='notice__form ' onSubmit={this.handleSubmit}
              autoComplete='off'>
          <fieldset className='notice__header' >
            <legend>Фотография квартиры (для карты)</legend>
            <div className='notice__photo col-12'>
              <div className='upload'>
                <div className='notice__preview'>
                  <img src={this.state.preview} alt='Аватар пользователя' width='40' height='44' />
                </div>
                <Input name='avatar' type='file' id='avatar' onChange={this.handlePhotoChange} hidden/>
                <label className='drop-zone' htmlFor='avatar'>Загрузите или&nbsp;перетащите сюда фото</label>
                <p className='notice__info'>Заполните все обязательные поля, назначьте цену, загрузите фотографии.
                  Придумайте интересное описание. Оно сделает объявление более живым и привлекательным.
                  Получившееся объявление должно давать гостям полное представление о жилье.</p>
              </div>
            </div>
          </fieldset>

          <div className='col-6'>
            <Input
              name='address'
              value={this.state.address}
              label='Адрес'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <Input
              name='currentCoords'
              value={`x: ${x}; y: ${y}`}
              label='Координаты для карты'
            />
          </div>

          <div className='col-6'>
            <Input
              name='apartmentNumber'
              value={this.state.apartmentNumber}
              label='Номер квартиры'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <Input
              name='costPerDay'
              value={this.state.costPerDay}
              label='Цена/сут.'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <Input
              name='square'
              value={this.state.square}
              label='Площадь'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <Input
              name='roomNumber'
              value={this.state.roomNumber}
              label='Количество комнат'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <Input
              name='floor'
              value={this.state.floor}
              label='Этаж'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <Input
              name='status'
              value={this.state.status}
              label='Статус'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <TextArea
              name='description'
              value={this.state.description}
              label='Описание (не обязательно)'
              onChange={this.handleChange}
            />
          </div>

          <div className='form__element features form__element--wide col-6 mb-4'>
            <span className='features__title text-center mb-2'>Удобства</span>

            <div className='features__list'>
              <input type='checkbox' name='features-adding' value='wifi' id='feature-wifi-adding'
                     checked={this.state.wifi} onChange={this.handleCheckboxChange}
              />
              <label className={classNames('feature feature--wifi')} htmlFor='feature-wifi-adding'>
                Wi-Fi
              </label>

              <input type='checkbox' name='features-adding' value='dishwasher' id='feature-dishwasher-adding'
                     checked={this.state.dishwasher} onChange={this.handleCheckboxChange}
              />
              <label className={classNames('feature feature--dishwasher')} htmlFor='feature-dishwasher-adding'>
                Посудомоечная машина
              </label>

              <input type='checkbox' name='features-adding' value='parking' id='feature-parking-adding'
                     checked={this.state.parking} onChange={this.handleCheckboxChange}/>
              <label className={classNames('feature feature--parking')} htmlFor='feature-parking-adding'>
                Парковка
              </label>

              <input type='checkbox' name='features--adding' value='washer' id='feature-washer-adding'
                     checked={this.state.washer} onChange={this.handleCheckboxChange}
              />
              <label className={classNames('feature feature--washer')} htmlFor='feature-washer-adding'>
                Стиральная машина
              </label>

              <input type='checkbox' name='features-adding' value='elevator' id='feature-elevator-adding'
                     checked={this.state.elevator} onChange={this.handleCheckboxChange}
              />
              <label className={classNames('feature feature--elevator')} htmlFor='feature-elevator-adding'>
                Лифт
              </label>

              <input type='checkbox' name='features-adding' value='conditioner' id='feature-conditioner-adding'
                     checked={this.state.conditioner} onChange={this.handleCheckboxChange}
              />
              <label className={classNames('feature feature--conditioner')} htmlFor='feature-conditioner-adding'>
                Кондиционер
              </label>
            </div>

          </div>

          <div className='form__element form__element--submit'>
            <button className='form__submit button-submit' type='submit'>Опубликовать</button>
          </div>
        </form>
      </div>
    );
  }
}