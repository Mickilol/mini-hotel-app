import * as React from 'react';

export class Footer extends React.Component {
  render() {
    return (
      <footer className='footer container'>
        <div className='center-wrapper'>
          <section className='footer-logo'>
            Created by Danila Matyushin &copy; 2018
          </section>
          <nav className='footer-social'>
            <a href='https://vk.com/daniel_matyushin' className='footer-social-item footer-social-item-vk'
               target='_blank' rel='nofollow'>VK</a>
          </nav>
        </div>
      </footer>
    );
  }

}