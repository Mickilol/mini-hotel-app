import * as React from 'react';
import { Link } from 'react-router-dom';
import Button from '../../Shared/components/Buttons/Button';
import { lazyInject } from '../../IoC';
import { LoginStore } from '../../Authorization/modules/Login/LoginStore';


export class Header extends React.Component {

  @lazyInject(LoginStore)
  private readonly loginStore: LoginStore;

  render() {
    const { isAdmin } = this.loginStore;

    return (
      <header className='col-12 navbar text-right'>
        <h1 hidden>Keksobooking. Кексы по соседству</h1>

        <Link to='/'>
          <Button className='mb-3'>
            Главная
          </Button>
        </Link>

        {isAdmin &&
          <Link to='/bookkeeping'>
            <Button className='mb-3'>
              Отчетность
            </Button>
          </Link>
        }

        <div>
          <Button onClick={() => this.loginStore.logout()}>
            Выйти
          </Button>
        </div>

      </header>
    );
  }

}