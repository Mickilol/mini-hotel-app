import * as React from 'react';
import Select from '../../Shared/components/Inputs/Select';
import TextArea from '../../Shared/components/Inputs/TextArea/TextArea';
import Button from '../../Shared/components/Buttons/Button';
import { IAddReviewPayload } from '../stores/DashboardStore';
import moment from 'moment';

interface IProps {
  userId: number;
  reviews: any[];
  apartments: any[];
  onAddReview: (payload: IAddReviewPayload) => void;
}

interface IState {
  apartmentId: number;
  message: string;
  score: number;
}

export default class Reviews extends React.Component<IProps, IState>  {

  constructor(props)  {
    super(props);

    this.state = {
      apartmentId: -1,
      message: '',
      score: 0,
    };
  }

  handleChange = ({name, value}) => {
    this.setState({[name]: value} as any);
  };

  handleSend = () => {
    const { apartmentId, message, score } = this.state;

    const payload: IAddReviewPayload = {
      apartmentId: Number(apartmentId),
      userId: this.props.userId,
      createDate: moment(new Date()).format('YYYY-MM-DD'),
      message,
      score: Number(score),
    };

    this.props.onAddReview(payload);
  };

  render() {
    return (
      <div className='notice reviews row pt-0'>
        <h2 className='notice__title mb-3'>Отзывы</h2>

        <div className='reviews__list col-12 mb-3'>
          {this.props.reviews.map((element, id) => (
            <div className='reviews__list__item' key={id}>
              <div className='photo'>
                <img src='muffin.png' width='70' height='70'/>
              </div>
              <div className='content'>
                <div className='content__header'>
                  <div className='author-name'>
                    <span>{element.name}</span> (Квартира: {element.address})
                  </div>
                  <div className='score'>
                    {element.score}/10
                  </div>
                </div>
                <div className='content__bottom'>
                  {element.message}
                </div>
              </div>
            </div>
          ))}
        </div>

        <div className='col-12 reviews__adding'>
          <div className='reviews__adding__title'>
            Оставить отзыв
          </div>

          <div className='reviews__adding__data'>
            <div className='reviews__adding__data__apartment'>
              <Select
                label='Квартира'
                name='apartmentId'
                options={this.props.apartments.map(element => ({
                  value: element.offer.id, label: element.offer.address,
                }))}
                onChange={this.handleChange}
              />
            </div>
            <div className='reviews__adding__data__score'>
              <Select
                label='Оценка'
                name='score'
                options={[
                  {value: 1, label: '1'},
                  {value: 2, label: '2'},
                  {value: 3, label: '3'},
                  {value: 4, label: '4'},
                  {value: 5, label: '5'},
                  {value: 6, label: '6'},
                  {value: 7, label: '7'},
                  {value: 8, label: '8'},
                  {value: 9, label: '9'},
                  {value: 10, label: '10'},
                ]}
                onChange={this.handleChange}
              />
            </div>
            <div className='reviews__adding__data__message'>
            <TextArea
              label='Сообщение'
              name='message'
              onChange={this.handleChange}
            />
            </div>
          </div>

          <div className='reviews__adding__send-button'>
            <Button onClick={this.handleSend} name='submit'>
              Отправить
            </Button>
          </div>
        </div>
      </div>
    );
  }
}