import * as React from 'react';
import Select from '../../Shared/components/Inputs/Select';
import InputDate from '../../Shared/components/Inputs/InputDate';
import Input from '../../Shared/components/Inputs/Input';
import { lazyInject } from '../../IoC';
import { LoginStore } from '../../Authorization/modules/Login/LoginStore';
import { IAddRentPayload } from '../stores/DashboardStore';
import moment from 'moment';


interface IProps {
  apartments: any[];
  onAddRent: (payload: IAddRentPayload) => void;
}

interface IState {
  rentDate: string;
  apartmentId: number;
  userId: number;
  term: number;
  price: number;
  address: string;
  costPerDay: string;
}

export class Rent extends React.Component<IProps, IState> {

  @lazyInject(LoginStore)
  private readonly loginStore: LoginStore;

  constructor(props) {
    super(props);

    this.state = {
      rentDate: '',
      apartmentId: -1,
      userId: Number(this.loginStore.accountId),
      term: 0,
      price: 0,
      address: '',
      costPerDay: '',
    };

  }

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      apartmentId,
      userId,
      price,
    } = this.state;

    let {
      rentDate,
      term,
    } = this.state;

    term = Number(term);

    rentDate = moment(rentDate).format('YYYY-MM-DD');

    const payload = {
      rentDate,
      apartmentId,
      userId,
      term,
      price,
    } as IAddRentPayload;

    this.props.onAddRent(payload);
  };

  handleChange = async ({name, value}) => {
    await this.setState({[name]: value} as any);
    const apartment = this.props.apartments.find(element => element.offer.id === this.state.apartmentId);
    const costPerDay = apartment !== undefined ? apartment.offer.costPerDay : '';

    await this.setState({costPerDay});

    if (name === 'term') {
      const price = Number(costPerDay) * Number(this.state.term);

      this.setState({ price });
    }
  };

  render() {
    const { apartments } = this.props;


    return (
      <div className='notice row'>
        <h2 className='notice__title mb-3'>Аренда апартаментов</h2>

        <form className='notice__form ' onSubmit={this.handleSubmit} >
          <div className='col-6'>
            <Select
              label='Квартира'
              name='apartmentId'
              options={apartments.map(element => ({ value: element.offer.id, label: element.offer.address }))}
              onChange={this.handleChange}
            />
          </div>

          <div className='col-6'>
            <InputDate
              name='rentDate'
              onChange={this.handleChange}
              label='Дата аренды'
            />
          </div>

          <div className='col-4'>
            <Input
              label='Срок аренды'
              name='term'
              onChange={this.handleChange}
            />
          </div>

          <div className='col-4'>
            <Input
              label='Стоимость / сут.'
              value={this.state.costPerDay}
            />
          </div>

          <div className='col-4'>
            <Input
              label='Стоимость (всего)'
              value={this.state.price}
            />
          </div>

          <div className='form__element form__element--submit'>
            <button className='form__submit button-submit' type='submit'>Арендовать</button>
          </div>
        </form>
      </div>
    );
  }

}