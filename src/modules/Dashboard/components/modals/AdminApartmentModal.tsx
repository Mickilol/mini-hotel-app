import * as React from 'react';
import Button from '../../../Shared/components/Buttons/Button';
import ModalBase from '../../../Modals/components/ModalBase';
import { lazyInject } from '../../../IoC';
import { LoginStore } from '../../../Authorization/modules/Login/LoginStore';
import Input from '../../../Shared/components/Inputs/Input';
import {
  DashboardStore,
  Facilities,
  IDeleteApartPayload, IDeleteFacilitiesPayload,
  IUpdateApartPayload,
  IUpdateFacilitiesPayload,
} from '../../stores/DashboardStore';
import TextArea from '../../../Shared/components/Inputs/TextArea/TextArea';
import classNames from 'classnames';

interface IState {
  preview: string;
  id: number;
  roomNumber: string;
  square: number;
  floor: number;
  costPerDay: number;
  address: string;
  description: string;
  status: string;
  apartmentNumber: string;
  x: number;
  y: number;

  wifi: boolean;
  dishwasher: boolean;
  parking: boolean;
  washer: boolean;
  elevator: boolean;
  conditioner: boolean;
}

interface IProps {
  title: string;
  description?: string;
  data: {
    preview: {
      photo: string;
    };
    offer: {
      id: number;
      roomNumber: string;
      square: number;
      floor: number;
      costPerDay: number;
      address: string;
      description: string;
      status: string;
      apartmentNumber: string;
    };
    location: {
      x: number;
      y: number;
    };
  };
  facilities: Array<{
    apartmentId: number;
    name: string;
    value: string;
  }>;
  onClose?: () => void;
}

export default class AdminApartmentModal extends React.Component<IProps, IState> {

  @lazyInject(LoginStore)
  private readonly loginStore: LoginStore;

  @lazyInject(DashboardStore)
  private readonly dashboardStore: DashboardStore;

  constructor(props) {
    super(props);

    this.state = {
      preview: this.props.data.preview.photo,
      id: this.props.data.offer.id,
      roomNumber: this.props.data.offer.roomNumber,
      square: this.props.data.offer.square,
      floor: this.props.data.offer.floor,
      costPerDay: this.props.data.offer.costPerDay,
      address: this.props.data.offer.address,
      description: this.props.data.offer.description,
      status: this.props.data.offer.status,
      apartmentNumber: this.props.data.offer.apartmentNumber,
      x: this.props.data.location.x,
      y: this.props.data.location.y,

      wifi: this.props.facilities.find(element => element.name === 'wifi') !== undefined,
      dishwasher: this.props.facilities.find(element => element.name === 'кухня') !== undefined,
      parking: this.props.facilities.find(element => element.name === 'парковка') !== undefined,
      washer: this.props.facilities.find(element => element.name === 'стиралка') !== undefined,
      elevator: this.props.facilities.find(element => element.name === 'лифт') !== undefined,
      conditioner: this.props.facilities.find(element => element.name === 'кондиционер') !== undefined,
    };
  }

  handleChange = ({name, value}) => {
      this.setState({[name]: value } as any);
  };

  handleCheckboxChange = ({ target: {value} }) => {
     this.setState({[value]: !this.state[value]} as any);
  };

  handlePhotoChange = ({name, value}) => {
    const array = value.split('\\');
    const preview = array[array.length - 1];
    console.log(preview);
    this.setState({ preview });
  };

  handleSubmit = (e) => {
    e.preventDefault();

    const {
      id,
      roomNumber,
      address,
      description,
      status,
      apartmentNumber,
      preview,
    } = this.state;

    let {
      square,
      floor,
      costPerDay,
      x,
      y,
    } = this.state;

    square = Number(square);
    floor = Number(floor);
    costPerDay = Number(costPerDay);
    x = Number(x);
    y = Number(y);

    const payload = {
      id,
      roomNumber,
      square,
      floor,
      costPerDay,
      address,
      description,
      status,
      apartmentNumber,
      x,
      y,
      preview,
    } as IUpdateApartPayload;

    this.dashboardStore.updateApartment(payload);

    // Обработка удобств на добавление

    const facilitiesArray = ['wifi', 'dishwasher', 'parking', 'washer', 'elevator', 'conditioner'];

    const facilities = {
      wifi: this.state.wifi,
      dishwasher: this.state.dishwasher,
      parking: this.state.parking,
      washer: this.state.washer,
      elevator: this.state.elevator,
      conditioner: this.state.conditioner,
    };

    let updatedFacilities = [];

    for (const item of facilitiesArray) {
      if (facilities[item] === true) {
        updatedFacilities.push(Facilities[item]);
      }
    }

    // удаление дублирующихся
    updatedFacilities.forEach(item => {
      const duplicate = this.props.facilities.find(element => element.name === item);
      if (duplicate !== undefined) {
        updatedFacilities = updatedFacilities.filter(element => element !== duplicate.name);
      }
    });


    updatedFacilities.forEach(item => {
      const payload = {
        apartmentId: id,
        facilitiesName: item,
        value: '',
      } as IUpdateFacilitiesPayload;

      this.dashboardStore.updateApartmentsFacilities(payload);
    });

    // Обработка удобств на удаление

    const noMarkedFacilities = [];
    const toDeleteFacilities = [];

    for (const item of facilitiesArray) {
      if (facilities[item] !== true) {
        noMarkedFacilities.push(Facilities[item]);
      }
    }

    for (const item of noMarkedFacilities) {
      const toDelete = this.props.facilities.find(element => element.name === item);
      if (toDelete !== undefined) {
        toDeleteFacilities.push(toDelete.name);
      }
    }

    toDeleteFacilities.forEach(item => {
      const payload = {
        apartmentId: id,
        facilitiesName: item,
      } as IDeleteFacilitiesPayload;

      this.dashboardStore.deleteApartmentFacilities(payload);
    });

    this.props.onClose();
  };

  handleDelete = async () => {
    const { id } = this.state;

    const promises = this.props.facilities.map(async item => {
      const payload = {
        apartmentId: id,
        facilitiesName: item.name,
      } as IDeleteFacilitiesPayload;

      await this.dashboardStore.deleteApartmentFacilities(payload);
    });
    await Promise.all(promises);

    const payload = { id } as IDeleteApartPayload;

    this.dashboardStore.deleteApartment(payload);

    this.props.onClose();
  };


  public render() {
    const { data, onClose } = this.props;

    return (
      <ModalBase onRequestClose={onClose}>
        <ModalBase.Title>Редактирование</ModalBase.Title>
        <form onSubmit={this.handleSubmit}>
          <div className='col-12 d-flex justify-content-center preview-edit mb-3'>
            <div className='preview'>
              <img src={this.state.preview} alt=''/>
            </div>

            <Input name='avatar' type='file' id='avatar-edit' onChange={this.handlePhotoChange} hidden/>
            <label className='drop-zone' htmlFor='avatar-edit'>Загрузите или&nbsp;перетащите сюда фото</label>
          </div>

          <Input
            name='address'
            value={this.state.address}
            label='Адрес'
            onChange={this.handleChange}
          />

          <div className='row'>
            <div className='col-6'>
              <Input
                name='costPerDay'
                value={this.state.costPerDay}
                label='Цена/сут.'
                onChange={this.handleChange}
              />
            </div>

            <div className='col-6'>
              <Input
                name='square'
                value={this.state.square}
                label='Площадь'
                onChange={this.handleChange}
              />
            </div>
          </div>

          <Input
            name='apartmentNumber'
            value={this.state.apartmentNumber}
            label='Номер квартиры'
            onChange={this.handleChange}
          />

          <div className='row'>
            <div className='col-6'>
              <Input
                name='roomNumber'
                value={this.state.roomNumber}
                label='Количество комнат'
                onChange={this.handleChange}
              />
            </div>

            <div className='col-6'>
              <Input
                name='floor'
                value={this.state.floor}
                label='Этаж'
                onChange={this.handleChange}
              />
            </div>
          </div>

          <Input
            name='status'
            value={this.state.status}
            label='Статус'
            onChange={this.handleChange}
          />

          <div className='row'>
            <div className='col-6'>
              <Input
                name='x'
                value={this.state.x}
                label='Координата X (для карты)'
                onChange={this.handleChange}
              />
            </div>

            <div className='col-6'>
              <Input
                name='y'
                value={this.state.y}
                label='Координата Y (для карты)'
                onChange={this.handleChange}
              />
            </div>
          </div>

          <div className='features features-mobile row mb-4 justify-content-around'>
            <span className='col-12 text-center mb-2'>Удобства</span>

            <input type='checkbox' name='features-edit' value='wifi' id='feature-wifi-edit'
                   checked={this.state.wifi} onChange={this.handleCheckboxChange}
            />
            <label className={classNames('feature feature--wifi')} htmlFor='feature-wifi-edit'>
              Wi-Fi
            </label>

            <input type='checkbox' name='features-edit' value='dishwasher' id='feature-dishwasher-edit'
                   checked={this.state.dishwasher} onChange={this.handleCheckboxChange}
            />
            <label className={classNames('feature feature--dishwasher')} htmlFor='feature-dishwasher-edit'>
              Посудомоечная машина
            </label>

            <input type='checkbox' name='features-edit' value='parking' id='feature-parking-edit'
                   checked={this.state.parking} onChange={this.handleCheckboxChange}/>
            <label className={classNames('feature feature--parking')} htmlFor='feature-parking-edit'>
              Парковка
            </label>

            <input type='checkbox' name='features-edit' value='washer' id='feature-washer-edit'
                   checked={this.state.washer} onChange={this.handleCheckboxChange}
            />
            <label className={classNames('feature feature--washer')} htmlFor='feature-washer-edit'>
              Стиральная машина
            </label>

            <input type='checkbox' name='features-edit' value='elevator' id='feature-elevator-edit'
                   checked={this.state.elevator} onChange={this.handleCheckboxChange}
            />
            <label className={classNames('feature feature--elevator')} htmlFor='feature-elevator-edit'>
              Лифт
            </label>

            <input type='checkbox' name='features-edit' value='conditioner' id='feature-conditioner-edit'
                   checked={this.state.conditioner} onChange={this.handleCheckboxChange}
            />
            <label className={classNames('feature feature--conditioner')} htmlFor='feature-conditioner-edit'>
              Кондиционер
            </label>
          </div>

          <TextArea
            name='description'
            value={this.state.description}
            label='Описание'
            onChange={this.handleChange}
          />

          <Button className='dashboard-btn dashboard-btn--modal' type='submit' >
            Редактировать
          </Button>

          <div className='text--center'>
            <span className='modal__notice' onClick={this.handleDelete}>Удалить</span>
          </div>
        </form>
      </ModalBase>
    );
  }
}