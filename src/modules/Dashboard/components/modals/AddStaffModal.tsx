import * as React from 'react';
import Input from '../../../Shared/components/Inputs/Input';
import Button from '../../../Shared/components/Buttons/Button';
import { BookkeepingStore, IAddStaffPayload } from '../../stores/BookkeepingStore';
import { observer } from 'mobx-react';
import { lazyInject } from '../../../IoC';
import ModalBase from '../../../Modals/components/ModalBase';

interface IProps {
  onClose: () => void;
}

interface IState {
  lastName: string;
  name: string;
  secondName: string;
  position: string;
  salary: number;
  passportData: string;
  phone: string;
}

@observer
export default class AddStaffModal extends React.Component<IProps, IState> {

  @lazyInject(BookkeepingStore)
  private readonly store: BookkeepingStore;

  constructor(props) {
    super(props);

    this.state = {
      lastName: '',
      name: '',
      secondName: '',
      position: '',
      salary: 0,
      passportData: '',
      phone: '',
    };
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    const {
      lastName,
      name,
      secondName,
      position,
      passportData,
      phone,
    } = this.state;

    const payload: IAddStaffPayload = {
      lastName,
      name,
      secondName,
      position,
      passportData,
      phone,
      salary: Number(this.state.salary),
    };

    await this.store.addStaff(payload);
    this.props.onClose();
  };

  handleChange = ({ name, value }) => {
    this.setState({[name]: value} as any);
  };

  render() {
    return (
      <ModalBase onRequestClose={this.props.onClose}>
        <h2 className='modal__title'>
          Добавление персонала
        </h2>
        <form onSubmit={this.handleSubmit}>
          <Input
            name='lastName'
            label='Фамилия'
            placeholder='Введите фамилию'

            onChange={this.handleChange}
          />

          <Input
            name='name'
            label='Имя'
            placeholder='Введите имя'

            onChange={this.handleChange}
          />

          <Input
            name='secondName'
            label='Отчество'
            placeholder='Введите отчество'

            onChange={this.handleChange}
          />

          <Input
            name='position'
            label='Должность'
            placeholder='Введите должность'

            onChange={this.handleChange}
          />

          <Input
            name='salary'
            label='Зарплата'
            placeholder='Введите зарплату'

            onChange={this.handleChange}
          />

          <Input
            name='passportData'
            mask='9999 999999'
            label='Паспортные данные'
            placeholder='Введите серию и номер паспорта'

            onChange={this.handleChange}
          />

          <Input
            name='phone'
            mask='9-999-999-99-99'
            label='Телефон'
            placeholder='9-999-999-99-99'
            errorMessage='Логин или телефон уже занят'

            onChange={this.handleChange}
          />

          <Button className='dashboard-btn dashboard-btn--modal' type='submit'>
            Добавить
          </Button>
        </form>
      </ModalBase>
    );
  }
}