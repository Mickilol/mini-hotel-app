import * as React from 'react';
import Input from '../../../Shared/components/Inputs/Input';
import Button from '../../../Shared/components/Buttons/Button';
import { BookkeepingStore, IAddStaffPayload, IDeleteStaff, IUpdateStaffPayload } from '../../stores/BookkeepingStore';
import { observer } from 'mobx-react';
import { lazyInject } from '../../../IoC';
import ModalBase from '../../../Modals/components/ModalBase';

interface IProps {
  onClose: () => void;

  id: number;
  lastName: string;
  name: string;
  secondName: string;
  position: string;
  salary: number;
  passportData: string;
  phone: string;
}

interface IState {
  lastName: string;
  name: string;
  secondName: string;
  position: string;
  salary: number;
  passportData: string;
  phone: string;
}

@observer
export default class EditStaffModal extends React.Component<IProps, IState> {

  @lazyInject(BookkeepingStore)
  private readonly store: BookkeepingStore;

  constructor(props) {
    super(props);

    this.state = {
      lastName: this.props.lastName,
      name: this.props.name,
      secondName: this.props.secondName,
      position: this.props.position,
      salary: this.props.salary,
      passportData: this.props.passportData,
      phone: this.props.phone,
    };
  }

  handleSubmit = async (e) => {
    e.preventDefault();

    const {
      lastName,
      name,
      secondName,
      position,
      passportData,
      phone,
    } = this.state;

    const payload: IUpdateStaffPayload = {
      id: this.props.id,
      lastName,
      name,
      secondName,
      position,
      passportData,
      phone,
      salary: Number(this.state.salary),
    };

    await this.store.updateStaff(payload);
    this.props.onClose();
  };

  handleDeleteStaff = async () => {
    const payload: IDeleteStaff = {
      id: this.props.id,
    };

    await this.store.deleteStaff(payload);
  };

  handleChange = ({ name, value }) => {
    this.setState({[name]: value} as any);
  };

  render() {
    return (
      <ModalBase onRequestClose={this.props.onClose}>
        <h2 className='modal__title'>
          Редактирование карточки персонала
        </h2>
        <form onSubmit={this.handleSubmit}>
          <Input
            name='lastName'
            label='Фамилия'
            placeholder='Введите фамилию'
            value={this.state.lastName}
            onChange={this.handleChange}
          />

          <Input
            name='name'
            label='Имя'
            placeholder='Введите имя'
            value={this.state.name}
            onChange={this.handleChange}
          />

          <Input
            name='secondName'
            label='Отчество'
            placeholder='Введите отчество'
            value={this.state.secondName}
            onChange={this.handleChange}
          />

          <Input
            name='position'
            label='Должность'
            placeholder='Введите должность'
            value={this.state.position}
            onChange={this.handleChange}
          />

          <Input
            name='salary'
            label='Зарплата'
            placeholder='Введите зарплату'
            value={this.state.salary}
            onChange={this.handleChange}
          />

          <Input
            name='passportData'
            mask='9999 999999'
            label='Паспортные данные'
            placeholder='Введите серию и номер паспорта'
            value={this.state.passportData}
            onChange={this.handleChange}
          />

          <Input
            name='phone'
            mask='9-999-999-99-99'
            label='Телефон'
            placeholder='9-999-999-99-99'
            errorMessage='Логин или телефон уже занят'
            value={this.state.phone}
            onChange={this.handleChange}
          />

          <Button className='dashboard-btn dashboard-btn--modal' type='submit'>
            Редактировать
          </Button>

          <div className='col-12 text-center'>
            <span className='clickable clickable-label' onClick={this.handleDeleteStaff}>Удалить</span>
          </div>
        </form>
      </ModalBase>
    );
  }
}