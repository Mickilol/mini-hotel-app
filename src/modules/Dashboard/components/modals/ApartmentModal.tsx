import * as React from 'react';
import Button from '../../../Shared/components/Buttons/Button';
import ModalBase from '../../../Modals/components/ModalBase';
import { lazyInject } from '../../../IoC';
import { LoginStore } from '../../../Authorization/modules/Login/LoginStore';
import classNames from 'classnames';
import Input from '../../../Shared/components/Inputs/Input';

interface IProps {
  title: string;
  description?: string;
  data: {
    preview: {
      photo: string;
    };
    offer: {
      id: number;
      roomNumber: string;
      square: number;
      floor: number;
      costPerDay: number;
      address: string;
      description: string;
      status: string;
      apartmentNumber: string;
    };
    location: {
      x: number;
      y: number;
    };
  };
  facilities: Array<{
    apartmentId: number;
    name: string;
    value: string;
  }>;
  onClose?: () => void;
}

export default class ApartmentModal extends React.Component<IProps> {

  @lazyInject(LoginStore)
  private readonly loginStore: LoginStore;

  public render() {
    const { title, data, onClose } = this.props;

    return (
      <ModalBase onRequestClose={onClose}>
        <ModalBase.Title>{data.offer.address}</ModalBase.Title>
        <div>

          <div className='col-12 d-flex justify-content-center preview-edit mb-3'>
            <div className='preview'>
              <img src={data.preview.photo} alt=''/>
            </div>

          </div>

          <div className='row mb-3'>
            <div className='col-6 pl-4'>
              Стоимость за сутки:
            </div>
            <div className='col-6 pr-4 text-right'>
              {data.offer.costPerDay}
            </div>
          </div>

          <div className='row mb-3'>
            <div className='col-6 pl-4'>
              Количество комнат:
            </div>
            <div className='col-6 pr-4 text-right'>
              {data.offer.roomNumber}
            </div>
          </div>

          <div className='row mb-3'>
            <div className='col-6 pl-4'>
              Площадь:
            </div>
            <div className='col-6 pr-4 text-right'>
              {data.offer.square}
            </div>
          </div>

          <div className='row mb-3'>
            <div className='col-6 pl-4'>
              Этаж:
            </div>
            <div className='col-6 pr-4 text-right'>
              {data.offer.floor}
            </div>
          </div>

          <div className='features features-mobile row mb-3 pr-3 align-items-center'>
            <span className='col-6 pl-4 features__title--user'>Удобства:</span>

            <input type='checkbox' name='features-edit' value='wifi' id='feature-wifi' disabled/>
            <label className={classNames('feature feature--wifi', {
              hidden: this.props.facilities.find(element => element.name === 'wifi') === undefined,
            } )} htmlFor='feature-wifi-edit'>
              Wi-Fi
            </label>

            <input type='checkbox' name='features-edit' value='dishwasher' id='feature-dishwasher-edit'  disabled/>
            <label className={classNames('feature feature--dishwasher', {
              hidden: this.props.facilities.find(element => element.name === 'кухня') === undefined,
            } )} htmlFor='feature-dishwasher-edit'>
              Посудомоечная машина
            </label>

            <input type='checkbox' name='features-edit' value='parking' id='feature-parking-edit' disabled/>
            <label className={classNames('feature feature--parking', {
              hidden: this.props.facilities.find(element => element.name === 'парковка') === undefined,
            } )} htmlFor='feature-parking-edit'>
              Парковка
            </label>

            <input type='checkbox' name='features-edit' value='washer' id='feature-washer-edit' disabled/>
            <label className={classNames('feature feature--washer', {
              hidden: this.props.facilities.find(element => element.name === 'стиралка') === undefined,
            } )} htmlFor='feature-washer-edit'>
              Стиральная машина
            </label>

            <input type='checkbox' name='features-edit' value='elevator' id='feature-elevator-edit' disabled/>
            <label className={classNames('feature feature--elevator', {
              hidden: this.props.facilities.find(element => element.name === 'лифт') === undefined,
            } )} htmlFor='feature-elevator-edit'>
              Лифт
            </label>

            <input type='checkbox' name='features-edit' value='conditioner' id='feature-conditioner-edit' disabled/>
            <label className={classNames('feature feature--conditioner', {
              hidden: this.props.facilities.find(element => element.name === 'кондиционер') === undefined,
            } )} htmlFor='feature-conditioner-edit'>
              Кондиционер
            </label>
          </div>

          {data.offer.description !== '' &&
            <div className='row mb-3'>
              <div className='col-4 pl-4'>
                Описание:
              </div>
              <div className='col-8 pr-4 text-right'>
                {data.offer.description}
              </div>
            </div>
          }


        </div>
        <Button className='dashboard-btn dashboard-btn--modal' onClick={onClose}>OK</Button>
      </ModalBase>
    );
  }
}