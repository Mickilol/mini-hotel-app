import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './modules/Shared/style/index.scss';

import registerServiceWorker from './registerServiceWorker';
import RootRouting from './modules/RootRouting';
import { BrowserRouter } from 'react-router-dom';

ReactDOM.render(
  <div>
    <BrowserRouter>
      <RootRouting />
    </BrowserRouter>
  </div>,
  document.getElementById('root') as HTMLElement,
);

registerServiceWorker();
